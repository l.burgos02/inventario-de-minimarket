## PRESENTATION

Dear reader,
The following repository consists of a project that will present a solution to the common problems that occur in the inventory management of minimarkets. In our case, it will focus on a specific minimarket located near the area where we are studying. The implementation of this solution will be represented by an information system within the company.

## INTRODUCTION

In the world of industries and commercial life, an efficient way for smaller markets to thrive and last for years to come is to implement systems with a procedural order within the company. This aims to improve its efficiency, quality, and productivity in a particular field.

Currently, the ability to control stock is highly important and a necessity for companies, smaller businesses, and organizations within the same field. Additionally, depending on the amount of available products, it is possible to study and analyze which ones have the highest or lowest consumption rate, and gather information about their demand.

## PROBLEMS

Issues such as product losses can arise due to inadequate organization in the inventory or an excess of products without considering the official count of total products in stock. This is often due to the lack of a systematic order and manual errors, which can be catastrophic in the logistics area.

## SOLUTION

Given this situation, our goal is to implement an ERP information system for a minimarket inventory, with the intention of controlling and managing the amount of products within the company, thereby providing concrete data that can help maintain the products organized and at a favorable stock.


## SCOPE OF THE PROJECT

This project's reach takes two particular ERP modules into consideration. These correspond to Inventory and Order Management, which handles the information and management of stores, product stock, and others, and Finance, which pertains to the information and managements of purchases made by the company's providers and clients. Other ERP modules such as Human Resources Management and Customer Resources Management will not be considered in this project.


## PROCESS DESCRIPTION

The processes are organized and coordinated in the following order:

**Supplier Request:** This process is responsible for requesting products from the suppliers to replenish the minimarket's stock.

**Product Restocking:** In this process, based on the previously made supplier request, the received products are checked and added to the inventory.

**Product Registration:** This process involves registering the newly arrived products in a database.

**Product Classification:** The registered products are classified and organized in the customer's storage areas in this process.

**Inventory Management:** In this process, the minimarket employee monitors the inventory to identify any product shortages. If there are any, they communicate with the purchasing manager who is responsible for contacting the suppliers.

**Product Sales:** Once the products are available, customers can enter the minimarket, make their purchases, and take the products with them. If a product is sold and the inventory quantity decreases, it is noted in the inventory management process.

**After-sales Service:** This process ensures that the customer receives the correct product. If there is an issue, the customer can exchange it for the correct product, provided they present the receipt.


![](https://gitlab.com/l.burgos02/inventario-de-minimarket/-/blob/master/diagrama%20de%20procesos-Minimarket/process.jpg)


## INPUT, OUTPUT, PROCESSING, DOCUMENTS, AND DATA OUTPUT DESCRIPTION

### Supplier Request

**Input:** "Product need" is the input event for the supplier request process.

**Output:** "Product request received" is the output event for the supplier request process.

**Documents:** "Requested document list" is the input document, and "Confirmed product list" is the output document.

**Data Output:** "Confirmed product list" is considered a data output that enters the next process.


### Product Restocking

**Input:** "List of received products".

**Output:** "Product incorporation".

**Documents:** "Received product list" is the input document, and "Registered product list" along with "Database product update" are the output documents.

**Data Output:** "Received product list" is considered a data output that enters the next process.


### Product Registration

**Input:** "Product incorporation".

**Output:** "Product registered".

**Documents:** "Registered product list" is the input document, and "Product classification by aisle" is the output document.

**Data Output:** "Registered product list" is considered a data output that enters the next process.


### Product Classification

**Input:** "Product registered".

**Output:** "Product classified".

**Documents:** "Registered product list" is the input document, and "Product classification by aisle" is the output document.


### Inventory Management

**Input:** "Product classified" is an input event for the inventory management process.

**Output:** "Product available" is an output event for the inventory management process, as well as "Out of stock product" which is redirected to the "Supplier Request" process.

**Documents:** "Current product list" is the input document.

**Data Output:** "Database product update" is the data output present in the system.


### Product Sales

**Input:** "Customer enters" and "Product available".

**Output:** "Product sold".

**Documents:** "Products registered for payment" is the input system information, and "Invoice" is the output document.

**Data Output:** "Invoice" is considered a data output that enters the next process.


### After-sales Service

**Input:** "Product sold".

**Output:** "Sale completed" and "Successful product exchange" if something goes wrong.

**Documents:** "Invoice" is the input document, and "Rating" is the output document.

**Data Output:** "Rating" is considered a data output that enters the next process.


## SYSTEMS INVOLVED

**Database:** The database is responsible for storing, deleting, and updating products within the minimarket's inventory.

**Transaction System:** Information system that generates the invoice.


## CONCLUSION

In summary, an inventory system as an information system in a minimarket provides more efficient management, accurate stock control, process automation, and better data analysis, resulting in a more profitable business and an enhanced shopping experience for customers.


## REFERENCES

**Definition of ERP:**

https://campusvirtual.ufro.cl/pluginfile.php/1675238/mod_resource/content/3/ICC%20362%20-%20M1_03%20Paradigmas%20de%20SI.pdf 

**Process Description:**

https://campusvirtual.ufro.cl/pluginfile.php/1675280/mod_resource/content/3/ICC362%20-%20M4_02%20Nomenclatura%20BPM%20-%20Parte%20I.pdf

**BPM-Minimarket Link:**

https://drive.google.com/file/d/1M6vvTBkqoleCcQLb5_6xVnLycyUfDLdc/view?usp=sharing 

**Inventory Errors:**

https://blog.snuuper.com/errores-comunes-inventario 
